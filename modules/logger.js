exports.id = 'logger';
exports.version = '1';

exports.log = function(method,action,entityId,portalId,status) {
  var self = this;
  var Log = MODEL('log').schema;

  Log.create({method:method,action:action,entityId:entityId,portalId:portalId,status:status},function(err, log){

  });
}

exports.purgeLogs = function(portalId) {
  var self = this;
  var Log = MODEL('log').schema;

  Log.remove({portalId:portalId},function(err, logs){
    if (err) console.log(err);
  });
}
