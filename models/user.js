var mongoose = require('mongoose');
var userSchema = mongoose.Schema({
  alias: String,
  created: {
    type: Date,
    default: Date.now
  }
});
exports.schema = mongoose.model('user', userSchema);
exports.name = 'user';
