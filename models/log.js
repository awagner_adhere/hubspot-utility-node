var mongoose = require('mongoose');
var logSchema = mongoose.Schema({
  method: {
    type: String
  },
  action: {
    type: String
  },
  entityId: {
    type: String,
  },
  portalId: {
    type: String
  },
  status: {
    type: String
  },
  timestamp: {
    type: Date,
    default: Date.now
  }
});
exports.schema = mongoose.model('log', logSchema);
exports.name = 'log';
