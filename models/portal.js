var mongoose = require('mongoose');
var portalSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  apiKey: {
    type: String,
    required: true
  },
  hubId:  {
    type: Number,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  }
});
exports.schema = mongoose.model('portal', portalSchema);
exports.name = 'portal';
