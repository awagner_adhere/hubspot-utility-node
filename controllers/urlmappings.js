exports.install = function() {
	F.route('/portal/{portalId}/url-mappings', show_mappings);
  F.route('/portal/{portalId}/url-mappings', update_mappings, ['post']);
  F.route('/portal/{portalId}/url-mappings/delete/{mappingId}', delete_mapping);
};

function show_mappings(portalId) {
  var self = this;
  var Portal = MODEL('portal').schema;

  Portal.findById(portalId,function(err,portal){
    if (err) return self.json(err);
    self.repository.portal = portal;
    U.request('https://api.hubapi.com/url-mappings/v3/url-mappings?limit=100000&hapikey='+portal.apiKey,['get','json'],function(error,data,status,headers){
      if (err) return self.json(error);

      self.view('index',{mappings:JSON.parse(data)});
    });
  });
}

function update_mappings(portalId){
  var self = this,
      fields = self.module('parseInputs').parse(self.post),
      async = require("async"),
      asyncTasks = [];


  for (var key in fields.mapping) {
    if (fields.mapping.hasOwnProperty(key)) {
      var mapping = fields.mapping[key];
      if(mapping.delete === '1'){
        var del = function(k,f,m,callback){
          U.request('https://api.hubapi.com/url-mappings/v3/url-mappings/'+k+'?hapikey='+f.apiKey,['delete'],function(error,data,status,headers){
            self.module('logger').log('delete','url-mappings',k,portalId,status);
            callback();
          });
        };
        asyncTasks.push(del.bind(null,key,fields,mapping));
      }else if(mapping.dirty === '1'){
        var update = function(k,f,m,callback){
          U.request('https://api.hubapi.com/url-mappings/v3/url-mappings/'+k+'?hapikey='+f.apiKey,['put','json'],m,function(error,data,status,headers){
            self.module('logger').log('put','url-mappings',k,portalId,status);
            callback();
          });
        };
        asyncTasks.push(update.bind(null,key,fields,mapping));
      }
    }
  }
  async.parallel(asyncTasks, function(){
    self.redirect('/portal/'+portalId+'/url-mappings');
  });
}

function delete_mapping(portalId,mappingId){
  var self = this;
  var Portal = MODEL('portal').schema;

  Portal.findById(portalId,function(err,portal){
    if (err) return self.json(err);
    U.request('https://api.hubapi.com/url-mappings/v3/url-mappings/'+mappingId+'?hapikey='+portal.apiKey,['delete'],function(error,data,status,headers){
      if (err) return self.json(error);
      self.module('logger').log('delete','url-mappings',mappingId,portalId,status);
      self.redirect('/portal/'+portalId+'/url-mappings');
    });
  });
}
