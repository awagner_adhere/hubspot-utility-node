exports.install = function() {
	F.route('/portal/{portalId}', show_portal);
};

function show_portal(portalId) {
  var self = this;
  var Portal = MODEL('portal').schema;

  Portal.findById(portalId,function(err,portal){
    if (err) return self.json(err);
    self.repository.portal = portal;
    self.view('index');
  });
}
