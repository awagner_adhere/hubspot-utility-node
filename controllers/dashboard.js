exports.install = function() {
	F.route('/', view_dashboard);
	F.route('/edit/{portalId}', edit_portal);
	F.route('/save', add_portal, ['post']);
	F.route('/save/{portalId}', update_portal, ['post']);
	F.route('/delete/{portalId}', delete_portal, ['get']);
};

function view_dashboard() {
  var self = this;
  var Portals = MODEL('portal').schema;

  Portals.find(function(err, portals){
		if (err) return self.json(err);

    self.view('index',{portals:portals,portal:{}});
  });
}

function edit_portal(portalId) {
  var self = this;
  var Portal = MODEL('portal').schema;

	Portal.find(function(err, portals){
		if (err) return self.json(err);

	  Portal.findById(portalId,function(err, portal){
			if (err) return self.json(err);

	    self.view('index',{portals:portals,portal:portal});
	  });
	});
}

function add_portal() {
	var self = this;
  var Portal = MODEL('portal').schema;

	Portal.create(self.post,function(err, portal){
		if (err) return self.plain(err.message);

		self.redirect('/');
	});
}

function update_portal(portalId) {
	var self = this;
  var Portal = MODEL('portal').schema;

	Portal.update({_id:portalId},self.post,function(err, portal){
		if (err) return self.plain(err.message);

		self.redirect('/');
	});
}

function delete_portal(portalId) {
	var self = this;
  var Portal = MODEL('portal').schema;

	Portal.findById(portalId,function(err, portal){
		if (err) return self.json(err);

		portal.remove(function(){
			self.module('logger').purgeLogs(portalId);
		});

		self.redirect('/');
	});
}
