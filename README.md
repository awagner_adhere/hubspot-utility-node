# Hubspot Utility

This is a utility application that I wrote that will allow you to do some repetitive tasks in Hubspot with fewer clicks, and with greater accuracy.  It should make life a little easier.

### Installation

I am assuming that you already have a suitable version of Node and NPM installed. You don't? Well, go do that.  I will wait here.

Next you'll need a Mongo database to store some of your data.  You can go sign up for a MongoLabs (https://mongolab.com) and put your URL string in the config file.

Got it?  Okay. So just run `npm install` on the directory where you placed the repo.  Then to fire it up, run `node --harmony debug.js`.  Then go to http://localhost:8000/

### API Key vs OAuth Token

In creating this, I opted to use API keys for simplicity sake.  In the future, it would be worth the effort to use OAuth and use tokens to do everything, but for now, it's just not to that point.  This has the side effect of being limited to Pro and Enterprise portals only.  Sorry, basic. :(

### Things that are working

(I will move things from the wish list to this list as they are done and functional.)

1. Portal Creation (CRUD)
2. URL mappings
  - Updating multiple
  - Deleting singles
  - Delete multiple
3. Logging to DB

### Wish List

1. URL mappings
  - Restore deleted
2. Pages
  - Mass creation
  - Mass update
  - Mass publish/archive/unpublish
3. Contacts
  - CRUD
  - Add/remove from list
  - Update property values (transform based on values, using definable regex)
